import React from 'react';

import Input from '../input';

import './index.styl';

export default class extends Input {
	handleChange = e => {
		super.handleChange(e.target.value);
	};
	renderInput() {
		const {className, ...props} = this.props;
		return <div className="_Input__Input --Textarea">
			<textarea {...props} onChange={this.handleChange} value={this.props.value}/>
		</div>
	}
}