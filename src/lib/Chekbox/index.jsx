import React from 'react';

import classNames from 'classnames';

import './index.styl';

export default class TextInput extends React.Component {
	state = {
		value: false
	};
	static getDerivedStateFromProps(props, state) {
		return {
			value: !!props.value
		}
	}
	handleChange = e => {
		this.setState({
			value: e.target.checked
		});
		this.props.onChange && this.props.onChange(e.target.checked)
	};
	render() {
		const {className, ...props} = this.props;
		return <div className={classNames('_Input', '_CheckboxInput', {'-isChecked': this.state.value}, className)}>
			<input type="checkbox" onChange={this.handleChange} checked={this.state.value}/>
		</div>
	}
}
