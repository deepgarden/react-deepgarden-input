import React from 'react';

import classNames from 'classnames';

import './index.styl';

export default class TextInput extends React.Component {
	handleChange = e => {
		this.props.onChange && this.props.onChange(e.target.value)
	};
	render() {
		const {className, ...props} = this.props;
		return <div className={classNames('_Input', '_PasswordInput', className)}>
			<input {...props} type="password" onChange={this.handleChange} value={this.props.value}/>
		</div>
	}
}