import React from 'react';

import Text from './lib/Text';
import Password from './lib/Password';
import Textarea from './lib/Textarea';
import Select from './lib/Select';
import Checkbox from './lib/Chekbox';
import Image from './lib/Image';

export const types = {
	text: Text,
	password: Password,
	textarea: Textarea,
	checkbox: Checkbox,
	select: Select,
	image: Image
};

const Input = props => {
	const {type, ...restProps} = props;
	const Component = types[type] || Text;
	return Component && <Component {...restProps}/>
};

Input.extend = function (type, component) {
	types[type] = component;
};

export default Input;